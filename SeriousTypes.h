#ifndef SERIOUSTYPES_H
  #define SERIOUSTYPES_H

#include <Arduino.h>

struct SeriousColor {
  char r;
  char g;
  char b;

  SeriousColor()
  : r(0), g(0), b(0) { /*stub*/ }

  SeriousColor(char r, char g, char b)
  : r(r), g(g), b(b) { /*stub*/ }

  String toString() {
    return String("{ r: ") + String(r) + String(", g: ") + String(g) + String(", b: ") + String(b) + String(" }");
  }
};

#endif
