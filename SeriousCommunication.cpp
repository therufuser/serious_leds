#include "SeriousCommunication.h"

SeriousCommunication::SeriousCommunication(boolean debugOn)
: debugOn(debugOn) {
  if(debugOn && Serial)
    Serial.println("SeriousCommunication::SeriousCommunication()");
}

boolean SeriousCommunication::decodeMessage(String message) {
  if(debugOn) {
    Serial.print("SeriousCommunication::decodeMessage(): Decoding message ( ");
    Serial.print(message); Serial.println(" )...");
  }

  boolean success = false;

  if(message.length() >= 1) {
    if(debugOn)
      Serial.println("\tdecodeMessage(): Message may be a command!");

    success = true;

    switch(message[0]) {
      case 'd': {
        if(debugOn)
          Serial.println("\t\tdecodeMessage(): case 'd'");

        onToggleDebug();

        break;
      }

      case 'h': {
        if(debugOn)
          Serial.println("\t\tdecodeMessage(): case 'h'");

        onPrintHelp();

        break;
      }

      case 'r': {
        if(debugOn)
          Serial.println("\t\tdecodeMessage(): case 'r'");

        onReset();

        break;
      }


      case 't': {
        if(debugOn)
          Serial.println("\t\tdecodeMessage(): case 't'");

        if(message.length() >= 2)
          onSetDelayTime(map(message[1], 'A', 'z', 0, 100));
        else
          success = false;

        break;
      }


      case 'f': {
        if(debugOn)
          Serial.println("\t\tdecodeMessage(): case 'f'");

        if(message.length() >= 5)
          onEnableFade(SeriousColor(message[1], message[2], message[3]), message[4]);
        else
          success = false;

        break;
      }

      case 'w': {
        if(debugOn)
          Serial.println("\t\tdecodeMessage(): case 'w'");

        if(message.length() >= 7)
          onCreateWaves(SeriousColor(message[1], message[2], message[3]), message[4], message[5], message[6]);
        else
          success = false;

        break;
      }

      case 'g': {
        if(debugOn)
          Serial.print("\t\tdecodeMessage(): case 'g'");

        if(message.length() >= 6) {
          char count = message[1];
          SeriousColor cols[count];
          for(int i = 0; i < count; i++) {
            int dataIndex = 2 + i * 3;
            cols[i] = SeriousColor(message[dataIndex], message[dataIndex + 1], message[dataIndex + 2]);
          }
          onEnableGlitter(count, cols, message[2 + count * 3]);
        }
        else
          success = false;

        break;
      }

      case 's': {
        if(debugOn)
          Serial.print("\t\tdecodeMessage(): case 's'");

        if(message.length() >= 6) {
          char count = message[1];
          SeriousColor cols[count];
          for(int i = 0; i < count; i++) {
            int dataIndex = 2 + i * 3;
            cols[i] = SeriousColor(message[dataIndex], message[dataIndex + 1], message[dataIndex + 2]);
          }
          onEnableStroboscope(count, cols, message[2 + count * 3]);
        }
        else
          success = false;

        break;
      }

      case 'p': {
        if(debugOn)
          Serial.print("\t\tdecodeMessage(): case 'p'");

        if (message.length() >= 4) {
          int count = (message.length() - 1) / 3;
          SeriousColor cols[count];
          for(int i = 0; i < count; i++) {
            int dataIndex = 2 + i * 3;
            cols[i] = SeriousColor(message[dataIndex], message[dataIndex + 1], message[dataIndex + 2]);
          }
          onReceivePacket(count, cols);
        }
        else
          success = false;

        break;
      }


      default: {
        if(debugOn)
          Serial.print("\t\tdecodeMessage(): default:");

        onDefault();
        success = false;
      }
    }
  }

  if(debugOn) {
    Serial.print("SeriousCommunication::decodeMessage(): exit => ");
    if(success)
      Serial.println("SUCCESS");
    else
      Serial.println("FAILURE");
  }

  return success;
}

const char* SeriousCommunication::getGreeterString() {
  return greeterString;
}

const char* SeriousCommunication::getHelpString() {
  return helpString;
}
