#!/usr/bin/env python3

import asyncio
import datetime
import random
import websockets

commands = {
    'd': 'onToggleDebug()',
    'h': 'onPrintHelp()',
    'r': 'onReset()',

    't': 'onSetDelayTime(map(message[1], A, z, 0, 100))',

    'f': 'onEnableFade(SeriousColor(message[1], message[2], message[3]), message[4])',
    'w': 'onCreateWaves(SeriousColor(message[1], message[2], message[3]), message[4], message[5], message[6])',
    'g': 'onEnableGlitter(count, cols, message[2 + count * 3])',
    's': 'onEnableStroboscope(count, cols, message[2 + count * 3])',
    'p': 'onReceivePacket(count, cols)'
}

def decodeMessage(message):
    print("decodeMessage(): Decoding message ( %s )..."%(message));

    if len(message) >= 1 and message[0] in commands:
        print("decodeMessage(): Message may be a command! ( %s )"%(message[0]))
        print("decodeMessage(): Handlercall! ( %s )"%(commands[message[0]]))

async def echo(websocket, path):
    while True:
        message = await websocket.recv()
        decodeMessage(message)
        if len(message) >= 1 and message[0] in commands:
            await websocket.send("Handler: " + commands[message[0]])

start_server = websockets.serve(echo, 'localhost', 8765)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
