#ifndef SERIOUSSERIAL_H
  #define SERIOUSSERIAL_H

#include "SeriousCommunication.h"

/** Serielle Konsolenschittstelle für seriöse LED Streifen

  Bietet das seriöse LED Streifen Protokoll (LSP) über eine serielle Verbindung an.
**/
class SeriousSerial: public SeriousCommunication {
  private:
    int baudRate;
    boolean serialConnected;

    /**Unimplementierte Eventhandler

      Bitte nach dem einbinden irgendwann implementieren.
    **/
    void onToggleDebug();
    void onPrintHelp();
    void onReset();

    void onSetDelayTime(char time);

    void onEnableFade(SeriousColor col, char speed);
    void onCreateWaves(SeriousColor col, char count, char width, char speed);
    void onEnableGlitter(char count, SeriousColor* cols, char speed);
    void onEnableStroboscope(char count, SeriousColor* cols, char speed);
    void onReceivePacket(int len, SeriousColor* data);

    void onDefault();

  public:
    SeriousSerial(int baudRate = 9600, boolean debugOn = false);

    boolean initConnection();
    boolean isConnected();
    boolean tryReconnect();
    boolean severConnection();

    boolean onSerialEvent();
};

#endif
