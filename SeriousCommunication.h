#ifndef SERIOUSCOMMUNICATION_H
  #define SERIOUSCOMMUNICATION_H

#include <Arduino.h>

#include <SeriousTypes.h>

/** Seriöse, wenn auch abstrakte, Kommunikationsschnittstelle für
    addressierbare LED Streifen

  Stellt eine Verbindung zu einem Host zur verfügung.

  Vom Host empfangene Befehle werden in decodeMessage() an die virtuallen
  on*() Eventhandler delegiert.
  Die Funktionalität dieser Handler muss in abgeleiteten Klassen
  definiert werden.
**/
class SeriousCommunication {
  private:
    // Begrüßungstext, der nach erstellen der Verbindung ausgegeben wird
    const char* greeterString = "Seriöser LED Streifen - Ver. 0.3.4\n"
      "Hallo :),\n"
      "Das hier ist eine Konsole, auf der du\n"
      "einen angeschlossenen LED Streifen\n"
      "mit Hilfe von einigen Befehlen steuern kannst.\n\n"
      "Die Hilfe kannst du dir mit 'h' anzeigen lassen.\n";

    // Hilfetext, erläutert die Konsolenbefehle
    const char* helpString = "Seriöse Hilfe (LED Streifen v0.3.4)\n"
      "Folgende Befehle sind verfügbar:\n"
      "\td (toggle debug mode)"
      "\th (show help)\n"
      "\tr (reset display)\n"
      "\n"
      "\tt, uint8: time (set frame duration)\n"
      "\n"
      "\tf, rgb8: col, uint8: speed (enable fade)\n"
      "\tw, rgb8: col, uint8: count, uint8: width, uint8: speed (create waves)\n"
      "\tg, uint8: count, rgb8[]: cols, uint8: speed (enable glitter)\n"
      "\ts, uint8: count, rgb8[]: cols, uint8: speed (enable strobo)\n"
      "\tp, uint8: len, rgb[]: data (send color packet)\n";

  protected:
    boolean debugOn;

    // Befehlsdispatcher. Dekodiert den Befehl in 'message' und delegiert an Eventhandler
    boolean decodeMessage(String message);

    // Virtuelle Eventhandler, bitte in abgeleiteter Klasse überschreiben
    virtual void onToggleDebug() = 0; //d
    virtual void onPrintHelp() = 0; //h
    virtual void onReset() = 0; //r

    virtual void onSetDelayTime(char time) = 0; //t, uint8: time

    virtual void onEnableFade(SeriousColor col, char speed) = 0; //f, rgb8: col, uint8: speed
    virtual void onCreateWaves(SeriousColor col, char count, char width, char speed) = 0; //w, rgb8: col, uint8: count, uint8: width, uint8: speed
    virtual void onEnableGlitter(char count, SeriousColor* cols, char speed) = 0; //g, uint8: count, rgb8[]: cols, uint8: speed
    virtual void onEnableStroboscope(char count, SeriousColor* cols, char speed) = 0; //s, uint8: count, rgb8[]: cols, uint8: speed
    virtual void onReceivePacket(int len, SeriousColor* data) = 0; //p, uint8: len, rgb8[]: data

    virtual void onDefault() = 0;

    const char* getGreeterString();
    const char* getHelpString();

  public:
    SeriousCommunication(boolean debugOn = false);

    // Stellt eine Verbindung zu einem seriösen Server her
    virtual boolean initConnection() = 0;
    // Prüft, ob noch eine Verbindung besteht
    virtual boolean isConnected() = 0;
    // Versucht, die Verbindung wiederherzustellen. 'true' bei Erfolg
    virtual boolean tryReconnect() = 0;
    // Trennt die Verbindung zum Server
    virtual boolean severConnection() = 0;
};

#endif
