#include <SeriousSerial.h>

SeriousCommunication* commInterface;

void setup() {
  commInterface = new SeriousSerial(9600, true);
  commInterface->initConnection();
}

void loop() {
}

void serialEvent() {
  ((SeriousSerial*)commInterface)->onSerialEvent();
}

/** Eventhandler der seriellen Konsole

  Sind hier erstmal stubs.
  Alles, was in diesen Funktionen steht, wird ausgeführt, sobald der entsprechende Befehl über die Konsole kommt.
**/
void SeriousSerial::onToggleDebug() {
  Serial.println("SeriousSerial::onToggleDebug()");
}

void SeriousSerial::onPrintHelp() {
  Serial.println("SeriousSerial::onPrintHelp()");
}

void SeriousSerial::onReset() {
  Serial.println("SeriousSerial::onReset()");
}

void SeriousSerial::onSetDelayTime(char time) {
  Serial.println("SeriousSerial::onSetDelayTime()");
}

void SeriousSerial::onEnableFade(SeriousColor col, char speed) {
  Serial.println("SeriousSerial::onEnableFade()");
}

void SeriousSerial::onCreateWaves(SeriousColor col, char count, char width, char speed) {
  Serial.println("SeriousSerial::onCreateWaves()");
}

void SeriousSerial::onEnableGlitter(char count, SeriousColor* cols, char speed) {
  Serial.println("SeriousSerial::onEnableGlitter()");
}

void SeriousSerial::onEnableStroboscope(char count, SeriousColor* cols, char speed) {
  Serial.println("SeriousSerial::onEnableStroboscope()");
}

void SeriousSerial::onReceivePacket(int len, SeriousColor* data) {
  Serial.println("SeriousSerial::onReceivePacket()");
}

void SeriousSerial::onDefault() {
  Serial.println("SeriousSerial::onDefault()");
}
