#include <Adafruit_NeoPixel.h>
#include <SeriousLEDStrip.h>

SeriousLEDStrip* ledStrip;

void setup() {
  ledStrip = new SeriousLEDStrip(100, 2);
}

int count = 0;
void loop() {
  if(++count == 80) {
    ledStrip->startFade(SeriousColor(192, 32, 120), 10);
  } else if(count == 160) {
    ledStrip->startWaves(SeriousColor(64, 12, 255), 10, 3, 12);
  } else if(count == 240) {
    ledStrip->startGlitter(1, new SeriousColor(225, 130, 32), 16);
  } else if(count == 640) {
    ledStrip->startStroboscope(1, new SeriousColor(225, 130, 32), 20);
    count = 0;
  }

  ledStrip->onLoop();
}
