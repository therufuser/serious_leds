#include <Adafruit_NeoPixel.h>
#include <SeriousLEDStrip.h>

#include <SeriousSerial.h>

#define DEBUG
#define PIN            2
#define NUMPIXELS      100

boolean debugOn = true;

SeriousLEDStrip* ledStrip = NULL;
SeriousCommunication* commInterface = NULL;

void setup() {
  commInterface = new SeriousSerial();
  commInterface->initConnection();

  #ifdef DEBUG
    Serial.println("setup(): enter");
  #endif

  ledStrip = new SeriousLEDStrip(100, 2);

  #ifdef DEBUG
    Serial.println("setup(): exit");
  #endif
}

int count = 0;
void loop() {
  if(++count == 40) {
    ledStrip->startFade(SeriousColor(192, 32, 120), 20);
  } else if(count == 80) {
    ledStrip->startWaves(SeriousColor(64, 12, 255), 10, 3, 12);
  } else if(count == 120) {
    ledStrip->startGlitter(1, new SeriousColor(225, 130, 32), 16);
  } else if(count == 160) {
    ledStrip->startStroboscope(1, new SeriousColor(225, 130, 32), 16);
    count = 0;
  }

  //while(!commInterface->isConnected())
  //    commInterface->tryReconnect();

  ledStrip->onLoop();
}

void serialEvent() {
  if(!((SeriousSerial*)commInterface)->onSerialEvent())
    Serial.println("Error int onSerialEvent()");
}

/** Implementierung der Eventhandler für die serielle Schnittstelle

  siehe SeriousSerial.h
**/
void SeriousSerial::onToggleDebug() {
  debugOn = !debugOn;

  if(debugOn)
    Serial.println("Debug mode on!");
  else
    Serial.println("Debug mode off!");
}

void SeriousSerial::onPrintHelp() {
  Serial.println(getHelpString());
}

void SeriousSerial::onReset() {
  //Sweeper::resetSweeps();

  if(debugOn) {
    Serial.println("Reset sweeps!");
  }
}

void SeriousSerial::onSetDelayTime(char time) {
  ledStrip->setFrameTime(time);

  if(debugOn) {
    Serial.print("Refresh interval set to "); Serial.print(time); Serial.println(" milliseconds.");
  }
}

void SeriousSerial::onEnableFade(SeriousColor col, char speed) {
  ledStrip->startFade(col, map(speed, 0, 255, 300, 5));

  if(debugOn) {
    Serial.println("Enabled fading!");
  }
}

void SeriousSerial::onCreateWaves(SeriousColor col, char count, char width, char speed) {
  ledStrip->startWaves(col, count, map(width, 0, 255, 0, 100), map(speed, 0, 255, 300, 5));

  if(debugOn) {
    Serial.print("Created "); Serial.print(count); Serial.println(" new waves.");
  }
}

void SeriousSerial::onEnableGlitter(char count, SeriousColor* cols, char speed) {
  ledStrip->startGlitter(count, cols, map(speed, 0, 255, 300, 5));

  if(debugOn) {
    Serial.println("Enabled glitter!");
  }
}

void SeriousSerial::onEnableStroboscope(char count, SeriousColor* cols, char speed) {
  ledStrip->startStroboscope(count, cols, map(speed, 0, 255, 300, 5));

  if(debugOn) {
    Serial.println("Enabled strobo!");
  }
}

void SeriousSerial::onReceivePacket(int len, SeriousColor* data) {
  ledStrip->displayArray(len, data);

  if(debugOn) {
    Serial.println("Received data:");
    for(int i = 0; i < len; i++)
      Serial.println(String("\t") + data[i].toString());
  }
}

void SeriousSerial::onDefault() {
  // Do nothing.
}
