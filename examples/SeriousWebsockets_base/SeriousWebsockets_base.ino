#include <WiFi.h>
#include <SeriousWebsocket.h>

WiFiServer* server;
SeriousCommunication* commInterface;

const char* ssid = "yourNetworkName";
const char* password =  "yourNetworkPassword";

void setup() {
  Serial.begin(115200);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }

  Serial.println("Connected to the WiFi network");
  Serial.println(WiFi.localIP());

  server = new WiFiServer(80)
  server.begin();
  delay(100);

  commInterface = new SeriousWebsocket(server, true);
  commInterface->initConnection();
}

void loop() {
  ((SeriousWebsocket*)commInterface)->onLoop();
}

/** Eventhandler des Websocket Interface

  Sind hier erstmal stubs.
  Alles, was in diesen Funktionen steht, wird ausgeführt, sobald der entsprechende Befehl über ein socket kommt.
**/
void SeriousWebsocket::onToggleDebug() {
  Serial.println("SeriousWebsocket::onToggleDebug()");
}

void SeriousWebsocket::onPrintHelp() {
  Serial.println("SeriousWebsocket::onPrintHelp()");
}

void SeriousWebsocket::onReset() {
  Serial.println("SeriousWebsocket::onReset()");
}

void SeriousWebsocket::onSetDelayTime(char time) {
  Serial.println("SeriousWebsocket::onSetDelayTime()");
}

void SeriousWebsocket::onEnableFade(SeriousColor col, char speed) {
  Serial.println("SeriousWebsocket::onEnableFade()");
}

void SeriousWebsocket::onCreateWaves(SeriousColor col, char count, char width, char speed) {
  Serial.println("SeriousWebsocket::onCreateWaves()");
}

void SeriousWebsocket::onEnableGlitter(char count, SeriousColor* cols, char speed) {
  Serial.println("SeriousWebsocket::onEnableGlitter()");
}

void SeriousWebsocket::onEnableStroboscope(char count, SeriousColor* cols, char speed) {
  Serial.println("SeriousWebsocket::onEnableStroboscope()");
}

void SeriousWebsocket::onReceivePacket(int len, SeriousColor* data) {
  Serial.println("SeriousWebsocket::onReceivePacket()");
}

void SeriousWebsocket::onDefault() {
  Serial.println("SeriousWebsocket::onDefault()");
}
