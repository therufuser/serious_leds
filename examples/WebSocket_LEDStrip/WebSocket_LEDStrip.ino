#include <WiFi.h>
#include <WebSocketClient.h>
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

class Sweeper {
  private:
    char startPos;
    char pos;
    char endPos;
    
    char width;
    uint32_t* colData;
    
    boolean tripBack = false;
    boolean done = false;
    
  public:
    Sweeper(uint32_t col, char width, char startPos, char endPos);
    ~Sweeper();
    
    static const int MAX_SWEEPS = 40;    
    static Sweeper** sweeps;
    
    static void resetSweeps();

    static void onLoop(Adafruit_NeoPixel* strip);
    
    static void makeSweeper(Sweeper** data, int len);
    
    void show(Adafruit_NeoPixel* strip);
    boolean isDone();
};

boolean debugOn = true;

#define PIN            12
#define NUMPIXELS      100
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
int delayval = 500; // delay for half a second
const char* ssid     = "Schniedelwutz69";
const char* password = "tyrannosaurus69";
char path[] = "/";
char host[] = "192.168.0.3:5678";
  
WebSocketClient webSocketClient;

// Use WiFiClient class to create TCP connections
WiFiClient client;

boolean createSocketConnection() {
  pixels.clear();
  pixels.show();
  
  boolean connection_success = false;
  while(!connection_success){
    // Connect to the websocket server
    if (client.connect("192.168.0.3", 5678)) {
      Serial.println("Connected");
      connection_success = true;
    } else
      Serial.println("Connection failed.");
  }

  boolean handshake_success = false;
  while(!handshake_success) {
    // Handshake with the server
    webSocketClient.path = path;
    webSocketClient.host = host;
    if (webSocketClient.handshake(client)) {
      Serial.println("Handshake successful");
      handshake_success = true;
    } else {
      Serial.println("Handshake failed.");  
    }
  }
  
  return connection_success && handshake_success;
}

void setup() {
  pixels.begin();
  Sweeper::resetSweeps();
  Serial.begin(115200);
  delay(10);

  // We start by connecting to a WiFi network

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  delay(5000);

  createSocketConnection();
}

int delayTime = 40;
void loop() {
  String data;

  if (client.connected()) {
    Sweeper::onLoop(&pixels);
    webSocketClient.getData(data);

  switch(data[0]) {
    case 'a': {
      Sweeper::makeSweeper(Sweeper::sweeps, Sweeper::MAX_SWEEPS);
      
      break;
    }
      
    case 'd': {
      debugOn = !debugOn;
      if(debugOn)
        Serial.println("Debug mode on!");
      else
        Serial.println("Debug mode off!");
      
      break;
    }
      
    case 't': {
      delayTime = map(data[1], 'A', 'z', 0, 100);
      if(debugOn) {
        Serial.print("Refresh interval set to "); Serial.print(delayTime); Serial.println(" milliseconds.");
      }
      
      break;
    }

    case 'r': {
      Sweeper::resetSweeps();
      
      if(debugOn) {
        Serial.println("Reset sweeps!");
      }
    }
    
    case 'c': {
      //maxCount = map(data[1], 'A', 'z', 20, 300);
      if(debugOn) {
        Serial.print("maxCount set to ");// Serial.println(maxCount);
      }
      
      break;
    }

    case 'h': {
      Serial.println("Welcome to whatever this is (Ver.: 0.04.20)"
        "Following commands are available:"
        "\n\ta (Sweeper)"
        "\n\td (toggle debug mode)"
        "\n\tt time:[A-z] (set frame duration)"
        "\n\tc count:[A-z]"
        "\n\th (show help)"
        "\n\tr (reset display)");
    }

    case 'p': {
      if (data.length() > 1) {
        Serial.print("Received data: ");
        Serial.println(data);
        for(int i=0; i < data.length() / 3; i++){
          // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
          pixels.setPixelColor(i, pixels.Color(data[i * 3],data[i * 3 + 1],data[i * 3 + 2]));
        }
      
        pixels.show(); // This sends the updated pixel color to the hardware.
        delay(delayTime); // Delay for a period of time (in milliseconds).
      }
    }

    default: {
      pixels.clear();
      pixels.show();
    }
  }    
    // capture the value of analog 1, send it along
    pinMode(1, INPUT);
    data = String(analogRead(1));
    
    webSocketClient.sendData(data);
    
  } else {
    // wait to fully let the client disconnect
    delay(500);
    Serial.println("Client disconnected. Reconnecting...");
    webSocketClient = WebSocketClient();
    boolean reconnected = false;
    reconnected = createSocketConnection();
  }  
}
