#ifndef SERIOUSSWEEPER_H
  #define SERIOUSSWEEPER_H

#include <Adafruit_NeoPixel.h>

#include "SeriousTypes.h"

class Sweeper {
  private:
    char startPos;
    char pos;
    char endPos;

    char width;
    uint32_t* colData;

    boolean tripBack = false;
    boolean done = false;

  public:
    Sweeper(SeriousColor col, char width, char startPos, char endPos);
    ~Sweeper();

    static const int MAX_SWEEPS = 40;
    static Sweeper** sweeps;

    static void resetSweeps();

    static void onLoop(Adafruit_NeoPixel* strip);

    static void makeSweeper(SeriousColor col, char width, char speed);

    void show(Adafruit_NeoPixel* strip);
    boolean isDone();
};

#endif
