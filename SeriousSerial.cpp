#include "SeriousSerial.h"

SeriousSerial::SeriousSerial(int baudRate, boolean debugOn)
: SeriousCommunication(debugOn), baudRate(baudRate) {}

boolean SeriousSerial::initConnection() {
  Serial.begin(baudRate);
  while(!Serial);
  Serial.flush();
  if(debugOn) {
    Serial.print("Connection established: "); Serial.print(baudRate, DEC); Serial.println(" baud");
    Serial.print("\tSerial.available(): "); Serial.println(Serial.available());
  }

  Serial.println(getGreeterString());
  Serial.flush();
  return Serial;
}

boolean SeriousSerial::isConnected() {
  return Serial;
}

boolean SeriousSerial::tryReconnect() {
  return initConnection();
}

boolean SeriousSerial::severConnection() {
  Serial.end();
  return !Serial;
}

String readToString() {
  if(Serial)
    Serial.println("readToString(): enter");

  String buffer;
  while(Serial.available() > 0) {
    buffer += char(Serial.read());
    delay(2); //Delay to wait for data
  }
  if(Serial) {
    Serial.println("\tread Buffer:");
    Serial.print("\tbuffer.length(): "); Serial.print(buffer.length()); Serial.println();
    if(buffer.length() > 0) {
      Serial.print("\t"); Serial.println(buffer);
    }
    Serial.println("readToString(): exit");
  }

  return buffer;
}

boolean SeriousSerial::onSerialEvent() {
  if(debugOn)
    Serial.println("SeriousSerial::onSerialEvent(): enter");

  boolean success = false;
  String message = readToString();
  if(debugOn) {
    Serial.print("\tonSerialEvent(): Got message! ( ");
    Serial.print(message.length()); Serial.println(" bytes )");
  }

  if(success = (message.length() > 0)) {
    decodeMessage(message);

    if(debugOn)
      Serial.println("\tonSerialEvent(): Dispatched message!");
  } else {
    if(debugOn)
      Serial.println("\tonSerialEvent(): Message too short! (0 bytes)");
  }

  if(debugOn)
    Serial.println("SeriousSerial::onSerialEvent(): exit");
  return success;
}
