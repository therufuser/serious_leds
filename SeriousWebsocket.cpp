#include "SeriousWebsocket.h"

SeriousWebsocket::SeriousWebsocket(WiFiServer* server, boolean debugOn)
: SeriousCommunication(debugOn), server(server) {}

boolean SeriousWebsocket::initConnection() {
  if(debugOn) {
    if(!Serial) {
      Serial.begin(115200);
      while(!Serial);
      Serial.flush();
    }
    Serial.print("Serial connection established: "); Serial.print(11520, DEC); Serial.println(" baud");
    Serial.print("\tSerial.available(): "); Serial.println(Serial.available());
    Serial.println(getGreeterString());
    Serial.flush();
  }

  client = server->available();
  if(debugOn)
    Serial.println("Awaiting HTTP Connection...");
  while(client.connected() && !webSocketServer.handshake(client)) {
    Serial.println("\tRetrying...");
    delay(10);
  }
  Serial.println("\tConnected!\n");

  return Serial;
}

boolean SeriousWebsocket::isConnected() {
  return client.connected();
}

boolean SeriousWebsocket::tryReconnect() {
  return initConnection();
}

boolean SeriousWebsocket::severConnection() {
  Serial.end();
  return !Serial;
}

boolean SeriousWebsocket::onLoop() {
  boolean success = false;

  if (client.connected()) {
    String data;
    data = webSocketServer.getData();

    if (data.length() > 0) {
       Serial.println(data);
       webSocketServer.sendData(data);
    }

    delay(10); // Delay needed for receiving the data correctly
    success = true;
  }

  return success;
}
