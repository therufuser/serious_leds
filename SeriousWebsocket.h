#ifndef SERIOUSWEBSOCKET_H
  #define SERIOUSWEBSOCKET_H

#include "SeriousCommunication.h"
#include <Wifi.h>
#include <WebSocketServer.h>

/** Websocketschittstelle für seriöse LED Streifen

  Bietet das seriöse LED Streifen Protokoll (LSP) über eine Websocket-Verbindung an.
**/
class SeriousWebsocket: public SeriousCommunication {
  private:
    WiFiServer server;
    WiFiClient client;
    WebSocketServer webSocketServer;

    /**Unimplementierte Eventhandler

      Bitte nach dem einbinden irgendwann implementieren.
    **/
    void onToggleDebug();
    void onPrintHelp();
    void onReset();

    void onSetDelayTime(char time);

    void onEnableFade(SeriousColor col, char speed);
    void onCreateWaves(SeriousColor col, char count, char width, char speed);
    void onEnableGlitter(char count, SeriousColor* cols, char speed);
    void onEnableStroboscope(char count, SeriousColor* cols, char speed);
    void onReceivePacket(int len, SeriousColor* data);

    void onDefault();

  public:
    SeriousWebsocket(WiFiServer* server, boolean debugOn = false);

    boolean initConnection();
    boolean isConnected();
    boolean tryReconnect();
    boolean severConnection();

    boolean onLoop();
};

#endif
