#include "SeriousSweeper.h"

Sweeper::Sweeper(SeriousColor col, char width, char startPos, char endPos)
: startPos(startPos), pos(startPos), endPos(endPos), width(width) {
  colData = new uint32_t[width + 1];

  for(int i = 0; i <= width; i++) {
    colData[i] = Adafruit_NeoPixel::Color(col.r, col.g, col.b);
  }
}

Sweeper::~Sweeper() {
  delete[] colData;
}

Sweeper** Sweeper::sweeps = NULL;

void Sweeper::resetSweeps() {
  if(sweeps != NULL) {
    for(int i = 0; i < MAX_SWEEPS; i++) {
      if(sweeps[i] != NULL)
        delete sweeps[i];

      sweeps[i] = NULL;
    }

    delete[] sweeps;
    sweeps = NULL;
  }

  sweeps = new Sweeper*[MAX_SWEEPS];
  for(int i = 0; i < MAX_SWEEPS; i++) {
    sweeps[i] = NULL;
  }
}

void Sweeper::onLoop(Adafruit_NeoPixel* strip) {
  if(sweeps != NULL) {
    for(int i = 0; i < Sweeper::MAX_SWEEPS; i++) {
      if(sweeps[i] != NULL) {
        sweeps[i]->show(strip);

        if(sweeps[i]->isDone()) {
          delete sweeps[i];
          sweeps[i] = NULL;
        }
      }
    }

    strip->show();
  } else {
    Serial.println("Error: Sweeper Array uninitialized.");
  }
}

void Sweeper::makeSweeper(SeriousColor col, char width, char speed) {
  if(sweeps != NULL) {
    char h = random(0, 255);

    for(int i = 0; i < MAX_SWEEPS; i++) {
      if(sweeps[i] == NULL) {
        sweeps[i] = new Sweeper(col, width, 50, 99);
        break;
      }
    }

    for(int i = 0; i < MAX_SWEEPS; i++) {
      if(sweeps[i] == NULL) {
        sweeps[i] = new Sweeper(col, width, 49, 0);
        break;
      }
    }
  } else {
    Serial.println("Error: Sweeper Array uninitialized.");
  }
}

void Sweeper::show(Adafruit_NeoPixel* strip) {
  if(!done) {
    for(int i = 0; i <= width; i++) {
      if(pos - i >= 0) {
        strip->setPixelColor(pos - i, colData[i]);
      }
      if(pos + i < strip->numPixels())
        strip->setPixelColor(pos + i, colData[i]);
      }
  }

  if(pos != endPos) {
    if(pos > endPos)
      pos--;
    else if(pos < endPos)
      pos++;
  } else { //move in other direction  //Move Sweeper into the invisible void and "kill" it
    //pos = ledCount + 2 * width;
    //done = true;
    if(!tripBack) {
      endPos = startPos;
      startPos = pos;

      tripBack = true;
    } else
      done = true;
  }
}

boolean Sweeper::isDone() {
  return done;
}
