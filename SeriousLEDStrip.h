#ifndef SERIOUSLEDSTRIP_H
	#define SERIOUSLEDSTRIP_H

#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

#include "SeriousTypes.h"
//#include <SeriousSweeper.h>

/**	LED Streifen Controller

	Kommuniniziert mit einem WS2812(B) Streifen, um verschiedene Lichteffekte
	abzuspielen.

	Interne Zustandsmaschine wird durch onLoop() aktualisiert. Der Zustand
	laesst sich ueber die start*() bzw. display*() Methoden wechseln.
**/
class SeriousLEDStrip {
	private:
		Adafruit_NeoPixel* pixels;

		boolean debugOn;
		enum State {
			FADE, WAVES, GLITTER, STROBO, ARRAY, NONE
		} currentState;

		int delayVal;
		int myTimer;
		char myCounter;
		int myValue;
		SeriousColor myCol;

		void resetStrip() {
			pixels->clear();
	  	pixels->show();
		}

	public:
		SeriousLEDStrip(Adafruit_NeoPixel* pixels, boolean debugOn = false);
		SeriousLEDStrip(int numPixels, char pixelPin, boolean debugOn = false);

		void onLoop();

		void startFade(SeriousColor fadeColor, int frameTime);
		void startWaves(SeriousColor col, int count, int size, int frameTime);
    void startGlitter(int count, SeriousColor* cols, int frameTime);
    void startStroboscope(int count, SeriousColor* cols, int frameTime);
    void displayArray(int len, SeriousColor* data);

		void setFrameTime(int frameTime);
};

#endif
