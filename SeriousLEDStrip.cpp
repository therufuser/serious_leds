#include "SeriousLEDStrip.h"

SeriousLEDStrip::SeriousLEDStrip(Adafruit_NeoPixel* pixels, boolean debugOn)
:	pixels(pixels), debugOn(debugOn), delayVal(0), currentState(NONE), myTimer(0), myCounter(0), myValue(0) {
  resetStrip();

  if(debugOn) {
    Serial.println("SeriousLEDStrip(Adafruit_NeoPixel*):");
    Serial.print("\tpixels->numPixels(): "); Serial.println(pixels->numPixels());
    Serial.print("\tpixels->getPin(): "); Serial.println(pixels->getPin());
    Serial.print("\tpixels->canShow(): "); Serial.println(pixels->canShow());
  }
}

SeriousLEDStrip::SeriousLEDStrip(int numPixels, char pixelPin, boolean debugOn)
: debugOn(debugOn), delayVal(30), currentState(NONE), myTimer(0), myCounter(0), myValue(0) {
  pixels = new Adafruit_NeoPixel(numPixels, pixelPin, NEO_GRB + NEO_KHZ800);
  pixels->begin();
  resetStrip();

  if(debugOn) {
    Serial.println("SeriousLEDStrip(int, int):");
    Serial.print("\tpixels->numPixels(): "); Serial.println(pixels->numPixels());
    Serial.print("\tpixels->getPin(): "); Serial.println(pixels->getPin());
    Serial.print("\tpixels->canShow(): "); Serial.println(pixels->canShow());
  }
}

void SeriousLEDStrip::onLoop() {
  if(debugOn) {
    Serial.println("SeriousLEDStrip::onLoop(): enter");
    Serial.print("\tpixels->numPixels(): "); Serial.println(pixels->numPixels());
    Serial.print("\tpixels->canShow(): "); Serial.println(pixels->canShow());

    switch(currentState) {
      case FADE: {
        Serial.println("\tcurrentState = FADE");
        break;
      } case WAVES: {
        Serial.println("\tcurrentState = WAVES");
        break;
      } case GLITTER: {
        Serial.println("\tcurrentState = GLITTER");
        break;
      } case STROBO: {
        Serial.println("\tcurrentState = STROBO");
        break;
      } case ARRAY: {
        Serial.println("\tcurrentState = ARRAY");
        break;
      } case NONE: {
        Serial.println("\tcurrentState = NONE");
      }
    }
  }

  switch(currentState) {
    case FADE: {
      if(++myCounter != 0) {
        pixels->clear();
        pixels->setBrightness(myCounter);
        for(int i = 0; i < pixels->numPixels(); i++)
          pixels->setPixelColor(i, myCol.r, myCol.g, myCol.b);
      }
      break;
    }

    case WAVES: {
      pixels->clear();

      if(myTimer++ >= 100) {
        myCounter--;

        if(myCounter > 0) {}
          //Sweeper::makeSweeper(myCol, myValue, 0);
        else
          currentState = NONE;

        myTimer = 0;
      }
      break;
    }

    case GLITTER: {
      pixels->clear();
      for(int i = 0; i < myCounter * 2; i++)
        pixels->setPixelColor(random(0, pixels->numPixels()), myCol.r, myCol.g, myCol.b);
      break;
    }

    case STROBO: {
      pixels->clear();
      if(++myCounter %= 2) {
        for(int i = 0; i < pixels->numPixels(); i++)
          pixels->setPixelColor(i, myCol.r, myCol.g, myCol.b);
      }

      break;
    }

    case ARRAY: {
      break;
    }
    case NONE: {
    }
  }

  pixels->show();
  delay(delayVal);

  if(debugOn)
  Serial.println("SeriousLEDStrip::onLoop(): exit");
}

void SeriousLEDStrip::startFade(SeriousColor fadeColor, int frameTime) {
  currentState = FADE;
  delayVal = frameTime;
  myCounter = 0;
  myValue = 0;
  myCol = fadeColor;

  if(debugOn) {
    Serial.println("We fadin' now:");
    Serial.print("\tr: "); Serial.println((unsigned int)fadeColor.r);
    Serial.print("\tg: "); Serial.println((unsigned int)fadeColor.g);
    Serial.print("\tb: "); Serial.println((unsigned int)fadeColor.b);
    Serial.print("\tframeTime: "); Serial.println(frameTime);
    Serial.println();
  }
}

void SeriousLEDStrip::startWaves(SeriousColor waveColor, int count, int size, int frameTime) {
  currentState = WAVES;
  delayVal = frameTime;
  myCounter = count;
  myValue = size;
  myCol = waveColor;

  if(debugOn) {
    Serial.println("We wavy now:");
    Serial.print("\tr: "); Serial.println((unsigned int)waveColor.r);
    Serial.print("\tg: "); Serial.println((unsigned int)waveColor.g);
    Serial.print("\tb: "); Serial.println((unsigned int)waveColor.b);
    Serial.print("\tcount: "); Serial.println(count);
    Serial.print("\tsize: "); Serial.println(size);
    Serial.print("\tframeTime: "); Serial.println(frameTime);
    Serial.println();
  }
}

void SeriousLEDStrip::startGlitter(int count, SeriousColor* cols, int frameTime) {
  currentState = GLITTER;
  delayVal = frameTime;
  myCounter = count;
  myValue = (int)cols;
  myCol = *cols;

  if(debugOn) {
    Serial.println("GLITTERFORCE!");
    Serial.print("\tcount: "); Serial.println(count);
    for(int i = 0; i < count; i++) {
      Serial.print("\tColor #"); Serial.print(i + 1); Serial.println(":");
      Serial.print("\t\tr: "); Serial.println((unsigned int)cols[i].r);
      Serial.print("\t\tg: "); Serial.println((unsigned int)cols[i].g);
      Serial.print("\t\tb: "); Serial.println((unsigned int)cols[i].b);
    }
    Serial.print("\tframeTime: "); Serial.println(frameTime);
    Serial.println();
  }
}

void SeriousLEDStrip::startStroboscope(int count, SeriousColor* cols, int frameTime) {
  currentState = STROBO;
  delayVal = frameTime;
  myCounter = count;
  myValue = (int)cols;
  myCol = *cols;

  if(debugOn) {
    Serial.println("STROBO!");
    Serial.print("\tcount: "); Serial.println(count);
    for(int i = 0; i < count; i++) {
      Serial.print("\tColor #"); Serial.print(i + 1); Serial.println(":");
      Serial.print("\t\tr: "); Serial.println((unsigned int)cols[i].r);
      Serial.print("\t\tg: "); Serial.println((unsigned int)cols[i].g);
      Serial.print("\t\tb: "); Serial.println((unsigned int)cols[i].b);
    }
    Serial.print("\tframeTime: "); Serial.println(frameTime);
    Serial.println();
  }
}

void SeriousLEDStrip::displayArray(int len, SeriousColor* data) {
  currentState = ARRAY;
  delayVal = 30;
  myCounter = 0;
  myValue = 0;
  myCol = SeriousColor();

  pixels->clear();
  //übergebene Pixelfarben sanft auf den Streifen auftragen
  for(int i = 0; i < len; i++)
    pixels->setPixelColor(i, data[i].r, data[i].g, data[i].b);

  if(debugOn) {
    Serial.println("Display an array!");
    Serial.print("\tlen: "); Serial.println(len);
    Serial.println();
  }
}

void SeriousLEDStrip::setFrameTime(int frameTime) {
  delayVal = frameTime;
}
